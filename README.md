# Desafio Imersão Fábrica de Software

Repositório para os códigos do desafio do projeto imersão Fábrica de Software 2019.2

**Área**: Desenvolvimento Mobile
**Projeto**: Ionic Kittens

O repositório source do projeto pode ser acessado [aqui](https://github.com/matheussousaf/projeto-imersao-2019-2) nele, foram apresentados e serão cobrados os seguintes itens:
* Consumir uma API REST;
* Padrões de projeto Ionic;
* Componentes Ionic;

**Deadline**: 30/08 às 14:00
